// SPDX-FileCopyrightText: 2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef xml_static_data_h
#define xml_static_data_h

#ifdef __cplusplus
extern "C" {
#endif

extern const char HPCSTRUCT_DTD[];
extern const char HPCSTRUCT_DTD_VERSION[];

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // xml_static_data_h
