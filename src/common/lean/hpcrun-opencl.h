// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef hpcrun_opencl_h
#define hpcrun_opencl_h


//******************************************************************************
// system includes
//******************************************************************************

#define CL_TARGET_OPENCL_VERSION 210
#include <CL/cl.h>

#endif
