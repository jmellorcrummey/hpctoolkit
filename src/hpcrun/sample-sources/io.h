// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef _HPCRUN_IO_H_
#define _HPCRUN_IO_H_

int hpcrun_metric_id_read(void);
int hpcrun_metric_id_write(void);

#endif
