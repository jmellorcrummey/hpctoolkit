// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef CUSTOM_INIT_H
#define CUSTOM_INIT_H

void hpcrun_do_custom_init(void);

#endif //CUSTOM_INIT_H
