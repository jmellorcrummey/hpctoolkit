# SPDX-FileCopyrightText: 2023-2024 Rice University
# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

# OS installation stage: Install build tools from the OS
FROM docker.io/ubuntu:22.04 as os-install

# Install the required OS packages
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  rm -f /etc/apt/apt.conf.d/docker-clean \
  && apt-get update -yqq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    ccache \
    clang \
    clang-11 \
    clang-12 \
    clang-13 \
    clang-14 \
    clang-15 \
    cmake \
    g++ \
    g++-10 \
    g++-11 \
    g++-12 \
    gcc \
    gcc-10 \
    gcc-11 \
    gcc-12 \
    git \
    libbz2-dev \
    libdw-dev \
    libelf-dev \
    libiberty-dev \
    liblzma-dev \
    libmpich-dev \
    libomp-dev \
    libpapi-dev \
    libpfm4-dev \
    libtbb-dev \
    libxerces-c-dev \
    libxxhash-dev \
    libyaml-cpp-dev \
    libzstd-dev \
    make  `# FIXME: See https://gitlab.com/hpctoolkit/hpctoolkit/-/issues/704` \
    mawk  `# FIXME: See https://gitlab.com/hpctoolkit/hpctoolkit/-/issues/704` \
    ninja-build \
    ocl-icd-opencl-dev \
    pkg-config \
    python3 \
    python3-dev \
    python3-docutils \
    python3-myst-parser \
    python3-pip \
    python3-sphinx \
    python3-venv \
    sed  `# FIXME: See https://gitlab.com/hpctoolkit/hpctoolkit/-/issues/704` \
    zlib1g-dev \
    $({ \
      case "$(dpkg --print-architecture)" in \
      amd64|arm64) echo 'nvidia-cuda-toolkit libnvidia-compute-550'; ;; \
      esac; \
    }) \
  && : # END

# Install a up-to-date versions of Meson, using a suitable Python
# Also install docutils for processing documentation.
RUN \
  python3 -m pip install \
    'meson>=1.1.0,<2' \
  && : # END


# From-source installation side-stage: Install (some) software from source
FROM os-install as src-install
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  rm -f /etc/apt/apt.conf.d/docker-clean \
  && apt-get update -yqq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    bzip2 \
  && : # END

# FIXME: Boost doesn't support DESTDIR, so we install to a prefix under /dest and hope nothing
# goes wrong. See https://github.com/boostorg/boost_install/issues/14 for discussion.
# We only need to do this because we need a visibility=global Boost installation.
# Tracking issue: https://gitlab.com/hpctoolkit/hpctoolkit/-/issues/815.
ADD https://boostorg.jfrog.io/artifactory/main/release/1.84.0/source/boost_1_84_0.tar.bz2 /usr/src/boost.tar.bz2
RUN --mount=type=bind,target=/tmp,from=scratch,rw \
  tar xjf /usr/src/boost.tar.bz2 -C /tmp \
  && cd /tmp/boost_1_84_0 \
  && ./bootstrap.sh --prefix=/dest/opt/boost --with-libraries=atomic,chrono,date_time,filesystem,graph,system,thread,timer \
  && ./b2 install visibility=global


# Composition stage: Produce what will become the final image
FROM os-install
COPY --from=src-install /dest /
COPY \
  ci/native/src/clang.ini \
  ci/native/src/clang11.ini \
  ci/native/src/clang12.ini \
  ci/native/src/clang13.ini \
  ci/native/src/clang14.ini \
  ci/native/src/clang15.ini \
  ci/native/src/gcc.ini \
  ci/native/src/gcc10.ini \
  ci/native/src/gcc11.ini \
  ci/native/src/gcc12.ini \
  /usr/share/meson/native/

RUN t=/usr/share/meson/native/hpctoolkit-ci.ini && rm -f "$t" \
  && echo "[built-in options]" >> "$t" \
  && echo "wrap_mode = 'nofallback'" >> "$t" \
  && echo "force_fallback_for = ['dyninst', 'libunwind', 'xed']" >> "$t" \
  && echo "" >> "$t" \
  && echo "[project options]" >> "$t" \
  && echo "auto_features = 'enabled'" >> "$t" \
  && { \
    case "$(dpkg --print-architecture)" in \
    amd64|arm64) ;; \
    *) echo "cuda = 'disabled'" >> "$t"; ;; \
    esac; \
  } \
  && echo "rocm = 'disabled'" >> "$t" \
  && echo "level0 = 'disabled'" >> "$t" \
  && echo "gtpin = 'disabled'" >> "$t" \
  && echo "" >> "$t" \
  && echo "[zstd:project options]" >> "$t" \
  && echo "lz4 = 'disabled'" >> "$t" \
  && : # EOF

# Set CMAKE_PREFIX_PATH to the paths for all the bits
ENV BOOST_ROOT=/opt/boost LD_LIBRARY_PATH=/opt/boost/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
